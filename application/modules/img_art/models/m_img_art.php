<?php
	
	class m_img_art extends CI_Model{
		
		public function get_images(){
			
			$result = $this->db->get('tb_images');
			
			return $result;
		}
		
		public function insert_image($data){
			
			$this->db->insert('tb_images', $data);
		
		}
	
	}

