<?php
	
	class img_art extends CI_Controller{
		
		public function __construct(){
			
			parent::__construct();
			$this->load->model('m_img_art');
			$this->load->library(array('upload','image_lib'));
		
		}
		
		public function index(){
			
			$data = array(
				
				'data_img' => $this->m_img_art->get_images(),
			
			);
			$this->load->view('v_img_art', $data);
		
		}
		
		public function upload(){
			
			$this->load->view('v_upload');
		
		}
		
		public function do_upload(){
		
			if(isset($_FILES['img_upload'])){
				
				$size = $_FILES['img_upload']['size'][0];
				
				if($size <=3200000 && $size != 0){
					$random = rand(000,999);
					
					$data=$_FILES['img_upload'];
					
					$total= count($data['name']);
					$data2=array();
					for($i=0; $i<$total; $i++)
					{
						$data2[]=array(
							'name'=>$random.$data['name'][$i],
							'type'=>$data['type'][$i],
							'tmp_name'=>$data['tmp_name'][$i],
							'error'=>$data['error'][$i],
							'size'=>$data['size'][$i],
						);

			
					}
					
					// echo "<pre>";
					// print_r($data);
					// die();
			
					$no=0;
					foreach($data2 as $row)
					{
						$config['upload_path'] = './_assets/images_new/';
						$config['allowed_types'] = 'gif|jpg|png|bmp';
						//$config['max_size']     = '3000';
						//$config['max_width']  	= '3000';
						//$config['max_height']  	= '3000';
						$this->load->library('multi_upload', $config);

						if($this->multi_upload->do_upload($data2[$no])){
						
							$image_data = $this->multi_upload->data();
							$this->image_lib->initialize(array(
							'image_library' => 'gd2',
							'source_image' => './_assets/images_new/'. $image_data['file_name'],
							'maintain_ratio' => true,
							'create_thumb' => true,
							'quality' => '100%',
							'width' => 300,
							'height' => 188,
							));
							if($this->image_lib->resize())
							{
							
							$output = $image_data['raw_name'].'_thumb'.$image_data['file_ext'];
						 
							}
							else
							{
							$data['error'] = $this->image_lib->display_errors();
							}

							$gambar = $image_data['file_name'];
							
							$data = array(
								
								'foto' => $gambar,
								'foto_thumb' => $output,
							
							);
							
							$this->m_img_art->insert_image($data);
						} 
						
					$no++;
					
					}
				}else{
				
					$this->session->set_flashdata('pesan', '<center><div><h3>Size of image is too large!</h3><br><h3>Please reupload your image..</h3></div></center>');
					redirect(base_url().'img_art/upload');
				
				}
					
			}
			
			redirect(base_url());
		}
		
		public function image_loop(){
			
			if($this->session->userdata('level') == null){
				
				redirect(base_url().'login');
			
			}else{
				
				$data['images'] = $this->m_img_art->get_images();
				$this->load->view('v_img_loop', $data);
			
			}
		
		}
		
	
	}

