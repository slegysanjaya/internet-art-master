<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE HTML>
<html>
<head>
<title>Internet Art</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<link rel="stylesheet" href="<?php echo base_url()?>_assets/css/style.css" type="text/css" media="all" />
<script src="<?php echo base_url()?>_assets/js/jquery-1.10.1.min.js"></script>
<script src="<?php echo base_url()?>_assets/js/modernizr.custom.js"></script>
<script src="<?php echo base_url()?>_assets/js/script.js"></script>
<link href='http://fonts.googleapis.com/css?family=Damion' rel='stylesheet' type='text/css'>
</head>
<body>
   <div class="container">	
			<header class="clearfix">
				<div class="header">
				<div class="wrap">
					
					<div class="logo">
					<a href="<?php echo base_url()?>"><h1>Internet Art</h1></a>
					
					</div>
					
					<div class="header_top">
						
						<div class="" style="background-color:#6699cc;padding:10px;border-radius:10px;">
								
								<a href="<?php echo base_url().'img_art/upload'?>">Upload Here</a>
							    
						</div>
					
					</div>				
			<div class="clear"></div>
	    </div>
			</div>
			</header>
			<div class="main">
				<?php if ($this->session->flashdata('pesan') != null): ?>
					<?php echo $this->session->flashdata('pesan'); ?>
				<?php endif ?>
				<div class="" style="height:250px;width:400px;position:relative;left:500px;top:20px;background:#6699cc;border:solid;border-radius:30px;">
					<h1 style="position:relative;left:80px;top:30px;">Choose your picture to upload here..</h1>
					<h1 style="position:relative;left:150px;top:30px;">Max size 3 Mb</h1>
					<form action="<?php echo base_url().'img_art/do_upload'?>" method="post" enctype="multipart/form-data" style="position:relative;left:110px;top:70px;">
						<input type="file" name="img_upload[]" required>
						<br>
						<br>
						<br>
						<input type="submit" name="submit" value="Upload" style="height:30px;width:170px;">
					</form>
				</div>
			</div>
		</div><!-- /container -->		
		<script src="<?= base_url()?>_assets/js/grid.js"></script>
		<script>
			$(function() {
				Grid.init();
			});
		</script>
	              <div class="copy_right">
						<p>All Rights Reseverd | Design by  <a href="http://w3layouts.com">W3Layouts</a></p>
		   		</div>
          </body>
</html>
