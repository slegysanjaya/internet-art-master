<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE HTML>
<html>
<head>
<title>Internet Art</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<link rel="stylesheet" href="<?php echo base_url()?>_assets/css/style.css" type="text/css" media="all" />
<script src="<?php echo base_url()?>_assets/js/jquery-1.10.1.min.js"></script>
<script src="<?php echo base_url()?>_assets/js/modernizr.custom.js"></script>
<script src="<?php echo base_url()?>_assets/js/script.js"></script>
<link href='http://fonts.googleapis.com/css?family=Damion' rel='stylesheet' type='text/css'>
</head>
<body>
   <div class="container">	
			<header class="clearfix">
				<div class="header">
				<div class="wrap">
					
					<div class="logo">
					<a href="<?php echo base_url()?>"><h1>Internet Art</h1></a>
					
					</div>
					
					<div class="header_top">
						
						<div class="" style="background-color:#6699cc;padding:10px;border-radius:10px;">
								
								<a href="<?php echo base_url();?>img_art/upload">Upload Here</a>
							    
						</div>
						<div class="" style="background-color:#6699cc;padding:10px;border-radius:10px;position:relative;top:10px;">
								
								<a href="<?php echo base_url().'img_art/image_loop'?>">Image Loop</a>
							    
						</div>
					</div>				
			<div class="clear"></div>
	    </div>
			</div>
			</header>
			<div class="main">
				<ul id="og-grid" class="og-grid">
				
				<?php foreach($data_img->result() as $key => $val):;?>
					<li>
						<a href="#" data-largesrc="<?php echo base_url().'_assets/images_new/'.$val->foto?>" data-title="Azuki bean" data-description="Swiss chard pumpkin bunya nuts maize plantain aubergine napa cabbage soko coriander sweet pepper water spinach winter purslane shallot tigernut lentil beetroot.">
							<img src="<?php echo base_url().'_assets/images_new/'.$val->foto_thumb?>" alt="img01"/>
						</a>
					</li>
				<?php endforeach;?>
					
				</ul>
			</div>
		</div><!-- /container -->		
		<script src="<?= base_url()?>_assets/js/grid.js"></script>
		<script>
			$(function() {
				Grid.init();
			});
		</script>
	              <div class="copy_right">
						<p>All Rights Reseverd | Design by  <a href="http://w3layouts.com">W3Layouts</a></p>
		   		</div>
          </body>
</html>
