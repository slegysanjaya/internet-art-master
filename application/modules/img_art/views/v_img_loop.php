<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE HTML>
<html>
<head>
<title>Internet Art</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<link rel="stylesheet" href="<?php echo base_url()?>_assets/css/style.css" type="text/css" media="all" />
<!-- <script src="<?php echo base_url()?>_assets/js/jquery-1.10.1.min.js"></script> -->
<script src="<?php echo base_url()?>_assets/js/modernizr.custom.js"></script>
<script src="<?php echo base_url()?>_assets/js/script.js"></script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js?ver=3.0.1"></script>
<script type="text/javascript" src="js/infinite-rotator.js"></script>
<link href='http://fonts.googleapis.com/css?family=Damion' rel='stylesheet' type='text/css'>
<style type="text/css">
	#rotating-item-wrapper {
	    position: relative;
	    width: 980px;
	    height: 347px;
	}
	.rotating-item {
	    display: none;
	    position: absolute;
	    padding-top: 10px;
	    left: 0;
	}
</style>
</head>
<body>
   <div class="container">	
			<header class="clearfix">
				<div class="header">
				<div class="wrap">
					<div class="logo">
					<a href="<?php echo base_url()?>"><h1>Internet Art</h1></a>
					
					</div>
					<div class="header_top">
						
						<div class="" style="background-color:#6699cc;padding:10px;border-radius:10px;">
								
								<center>
									<a href="<?php echo base_url().'login/logout'?>">Log out</a>
								</center>
							    
						</div>
	
					</div>				
			<div class="clear"></div>
	    </div>
			</div>
			</header>
			<div class="main">
				<center>
					<div id="rotating-item-wrapper">
						<?php foreach($images->result() as $key) : ?>
							<img class="rotating-item main"  src="<?php echo base_url(); ?>_assets/images_new/<?php echo $key->foto ?>"  width="1024" height="347" />
						<?php endforeach ?>
					</div>
				</center>
				
				<!--
					script untuk menampilkan loop image
				-->
			</div>
		</div><!-- /container -->		
		<script src="<?= base_url()?>_assets/js/grid.js"></script>
		<script>
			$(function() {
				Grid.init();
			});

			$(window).load(function() { //start after HTML, images have loaded
 
			    var InfiniteRotator =
			    {
			        init: function()
			        {
			            //initial fade-in time (in milliseconds)
			            var initialFadeIn = 800;
			 
			            //interval between items (in milliseconds)
			            var itemInterval = 3000;
			 
			            //cross-fade time (in milliseconds)
			            var fadeTime = 1500;
			 
			            //count number of items
			            var numberOfItems = $('.rotating-item').length;
			 
			            //set current item
			            var currentItem = 0;
			 
			            //show first item
			            $('.rotating-item').eq(currentItem).fadeIn(initialFadeIn);
			 
			            //loop through the items
			            var infiniteLoop = setInterval(function(){
			                $('.rotating-item').eq(currentItem).fadeOut(fadeTime);
			 
			                if(currentItem == numberOfItems -1){
			                    currentItem = 0;
			                }else{
			                    currentItem++;
			                }
			                $('.rotating-item').eq(currentItem).fadeIn(fadeTime);
			 
			            }, itemInterval);
			        }
			    };
			 
			    InfiniteRotator.init();
			 
			});
		</script>
	            <div class="copy_right">
					<p>All Rights Reseverd | Design by  <a href="http://w3layouts.com">W3Layouts</a></p>
		   		</div>
          </body>
</html>
