<?php
	
	class m_login extends CI_Model{
		
		public function do_login($data){
			
			$this->db->where('username', $data['username']);
			$this->db->where('password', $data['password']);
			$result = $this->db->get('tb_user');
			
			if($result->num_rows() == 1){
				
				$data_session['username'] = $result->row()->username;
				$data_session['password'] = $result->row()->password;
				$data_session['level'] = 'admin';
				$this->session->set_userdata($data_session);
				return true;
			
			}else{
				
				return false;
			
			}
		}
	
	}

?>