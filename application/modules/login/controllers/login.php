<?php
	
	class login extends CI_Controller{
		
		public function __construct(){
			
			parent::__construct();
			$this->load->model('m_login');
		
		}
		
		public function index(){
			
			if($this->session->userdata('level') == null){
			
				$this->load->view('v_login');
			
			}else{
			
				redirect(base_url());
				
			}
			
		
		}
		
		public function do_login(){
			
			$data = array(
				
				'username' => $this->input->post('username'),
				'password' => $this->input->post('password'),
			
			);
			
			$hasil = $this->m_login->do_login($data);
			
			if($hasil){
				
				redirect(base_url().'img_art/image_loop');
				
			}else{
				
				redirect(base_url().'login');
			
			}
		
		}
		
		public function logout(){
			
			$this->session->sess_destroy();
			redirect(base_url());
		
		}
	}

