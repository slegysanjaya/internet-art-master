-- phpMyAdmin SQL Dump
-- version 4.2.7.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 02, 2017 at 05:35 PM
-- Server version: 5.6.20
-- PHP Version: 5.5.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `db_internet_art`
--

-- --------------------------------------------------------

--
-- Table structure for table `tb_images`
--

CREATE TABLE IF NOT EXISTS `tb_images` (
`id_images` int(11) NOT NULL,
  `foto` text NOT NULL,
  `foto_thumb` text NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `tb_images`
--

INSERT INTO `tb_images` (`id_images`, `foto`, `foto_thumb`) VALUES
(1, '17020130728_124203.jpg', '17020130728_124203_thumb.jpg'),
(2, '339News-interna-5-considerazioni-2016-punto5.jpg', '339News-interna-5-considerazioni-2016-punto5_thumb.jpg'),
(3, '106Hikaru_no_Go_character_Fujiwara_no_Sai__12_.jpg', '106Hikaru_no_Go_character_Fujiwara_no_Sai__12__thumb.jpg'),
(4, '487Hikaru_no_Go_character_Fujiwara_no_Sai__12_.jpg', '487Hikaru_no_Go_character_Fujiwara_no_Sai__12__thumb.jpg'),
(5, '976Fujiwara_No_Sai_x2_by_Teefie_Chan.jpg', '976Fujiwara_No_Sai_x2_by_Teefie_Chan_thumb.jpg'),
(6, '90920160113_150422-1.jpg', '90920160113_150422-1_thumb.jpg'),
(7, '115Sai_by_snowbunnyluv.jpg', '115Sai_by_snowbunnyluv_thumb.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `tb_user`
--

CREATE TABLE IF NOT EXISTS `tb_user` (
`id_user` int(11) NOT NULL,
  `username` text NOT NULL,
  `password` text NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `tb_user`
--

INSERT INTO `tb_user` (`id_user`, `username`, `password`) VALUES
(1, 'danirahman', '12345');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_images`
--
ALTER TABLE `tb_images`
 ADD PRIMARY KEY (`id_images`);

--
-- Indexes for table `tb_user`
--
ALTER TABLE `tb_user`
 ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_images`
--
ALTER TABLE `tb_images`
MODIFY `id_images` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `tb_user`
--
ALTER TABLE `tb_user`
MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
